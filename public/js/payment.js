$(window).ready(function() {

	$('.shirts_input').on('change keyup', function() {
		var total = 0;
		var limit = parseInt($('#quantity_1').text());

		for(var i = 1; i < 5; i++ ) {
			total += parseInt($('input.shirts_input_'+i).val());
		}

		console.log(limit);

		if(total >= limit)  {
			alert('Only '+limit+' shirt(s) for this order!');
		}

	});
});