@extends('base')

@section('main')
<div class="detail" style="padding:20px;">
	<div class="user">
		<h2 class="title">Customer Detail</h2>
		<p>
			Name: {{ $user->name }}
			<br>
			Email: {{ $user->email }}
			<br>
			Login Via: {{ ucfirst($user->provider) }}
		</p>
	</div>

	<div class="order">
		<h2 class="title">Order Detail</h2>
		<p style="margin-bottom:0">ID: <span id="verifyID">{{ $ticket_payments->id }}</span></p>
		<p>
			Total: RM {{ $ticket_payments->amount}}
			<br>
			Time: {{ date('d/m/y', strtotime($ticket_payments->updated_at))}} @ {{date('H:i', strtotime($ticket_payments->updated_at))}}
		</p>
	</div>

	<div class="order">
		<h2 class="title">Order Summary</h2>
		<table class="table" width="100%">
			<tr>
				<th align="left" width="60%">Ticket Name</th>
				<th align="center" width="20%">Quantity</th>
				<th align="center" width="20%">Amount</th>
			</tr>
			@foreach ($orders as $order)
			<tr>
				<td align="left"><p style="margin-bottom: 0;">{{ $order->name }}</p></td>
				<td align="center"><p style="margin-bottom: 0;">{{ $order->quantity }}</p></td>
				<td align="center"><p style="margin-bottom: 0;">RM{{ $order->total }}</p></td>
			</tr>
			@endforeach
		</table>
		@if($ticket_payments->verified == false)
		<div id="verifyRedeem" class="acc-button btn btn-primary" style="width: 100%;">Press To Redeem</div>
		<div id="redeemed" class="acc-button btn btn-primary" style="width: 100%;background-color: #ef3f40; display:none">Tickets Redeemed @ {{ date('d/m/y H:i:s', strtotime($ticket_payments->verified_time))}}</div>
		@else
		<div class="acc-button btn btn-primary" style="width: 100%;background-color: #ef3f40">Tickets Redeemed @ {{ date('d/m/y H:i:s', strtotime($ticket_payments->verified_time))}}</div>
		@endif
	</div>
</div>
@endsection