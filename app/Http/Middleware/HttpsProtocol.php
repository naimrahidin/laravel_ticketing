<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class HttpsProtocol
{

	public function handle($request, Closure $next)
	{
		Request::setTrustedProxies([$request->getClientIp()],Request::HEADER_X_FORWARDED_ALL);
		if (!$request->server('HTTP_X_FORWARDED_PROTO') != 'https' && App::environment() === 'prod') {
			return redirect()->secure($request->getRequestUri());
		}

		return $next($request);
	}
}

?>