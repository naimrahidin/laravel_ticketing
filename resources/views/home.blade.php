@extends('base')

@section('main')
<div class="acc-wrapper">
	<!-- start of nav -->
	<div class="nav">
		<div class="logo logotrigger"><img src="/images/logo/logo.png" alt="Asia Comic Con"></div>
		<div class="nav-toggle" id="menu-toggle"><img src="/images/icon/toggle.png" alt="Menu"></div>
		<div class="navigation-mobile">
			<div class="close-menu" id="menu-close"><img src="/images/icon/close.png" alt="Close"></div>
			<div class="mobile-nav-wrapper">
				<!-- <div class="nav-button activities-trigger">Activities</div> -->
				<div class="nav-button about-trigger">About</div>
				<div class="nav-button artist-trigger">Artists</div>
				<!-- <div class="nav-button ticket-trigger">Ticket</div> -->
				<div class="nav-button gallery-trigger">Gallery</div>
				<div class="nav-button video-trigger">Video</div>
				<div class="nav-button booth-trigger">Booth</div>
			</div>
		</div>
		<div class="navigation-desktop">
			<!-- <div class="nav-button activities-trigger">Activities</div> -->
			<div class="nav-button about-trigger">About</div>
			<div class="nav-button artist-trigger">Artists</div>
			<!-- <div class="nav-button ticket-trigger">Ticket</div> -->
			<div class="nav-button gallery-trigger">Gallery</div>
			<div class="nav-button video-trigger">Video</div>
			<div class="nav-button booth-trigger">Booth</div>
		</div>
	</div>
	<!-- end of nav -->
	<div class="acc-header">
		<div class="logo"><img src="images/logo/logo.png" alt="Asia Comic Con 2019"></div>
	</div>
	<!-- ticket -->
	<div class="section bg-red" id="ticket">
		<h2 class="f-white">Ticket</h2>
		<div class="divider blue"></div>
		<div class="ticket-section">
			<div class="ticket-img"><img src="/images/ticket/ticket-price-list.png" alt="Ticket"></div>
		</div>
		<a href="/tickets"><div class="acc-button red">Purchase Now</div></a>
	</div>
	<!-- end of ticket -->
	<!-- ticket -->
	<div class="section bg-img" id="schedule">
		<div class="schedule-wrapper">
			<h2 class="f-white">Schedules</h2>
			<div class="divider blue"></div>
			<div class="ticket-section">
				<div class="ticket-img"><img src="/images/schedule/day1.png" alt="Day 1"></div>
				<div class="ticket-img"><img src="/images/schedule/day2.png" alt="Day 2"></div>
			</div>
		</div>
		<!-- <a href="/ticket.html"><div class="acc-button red">Purchase Now</div></a> -->
	</div>
	<!-- end of ticket -->
	<!-- start of event -->
	<div class="section bg-red" id="about">
		<h2 class="f-white">Event Info</h2>
		<div class="divider blue"></div>
		<div class="event-info-wrapper">
			<div class="event-img"><img src="/images/mvec.png" alt="Mid Valley Exhibition Centre"></div>
			<div class="event-info">
				<div class="logo"><img src="/images/logo/logo.png" alt="Asia Comic Con"></div>
				<h2 class="grid-title f-white">Asia Comic Con Malaysia</h2>
				<p class="f-white">The Unforgetable Pop Culture Experience is here!</p>
				<p class="f-blue bold">OCTOBER 26-27, 2019</p>
          <!-- <p class="">10:00am - 7:00pm
          	<br> (Massive queues expected, so be early!)</p> -->
          	<p class="f-green bold">MID VALLEY EXHIBITION CENTRE</p>
          	<a href="https://goo.gl/maps/Fp32faGdr622" target="_blank">
          		<div class="acc-button red">Google Maps</div>
          	</a>
          </div>
      </div>
  </div>
</div>
<!-- end of event  -->
<!-- artist -->
<div class="section bg-red" id="artist">
	<div class="artist">
		<h2 class="f-black">Artist</h2>
		<div class="divider red"></div>
		<div class="artist-wrapper">
			<div class="artist-img"><img src="/images/artist/ario.png" alt=""></div>
			<div class="artist-img"><img src="/images/artist/azu.png" alt=""></div>
			<div class="artist-img"><img src="/images/artist/dj-benkun.png" alt=""></div>
			<div class="artist-img"><img src="/images/artist/higashi.png" alt=""></div>
			<div class="artist-img"><img src="/images/artist/jason.png" alt=""></div>
			<div class="artist-img"><img src="/images/artist/kaori-kawabuchi.png" alt=""></div>
			<div class="artist-img"><img src="/images/artist/kaori-lalachan.png" alt=""></div>
			<div class="artist-img"><img src="/images/artist/kaos.png" alt=""></div>
			<div class="artist-img"><img src="/images/artist/kats.png" alt=""></div>
			<div class="artist-img"><img src="/images/artist/mika.png" alt=""></div>
			<div class="artist-img"><img src="/images/artist/minri.png" alt=""></div>
			<div class="artist-img"><img src="/images/artist/phnx.png" alt=""></div>
			<div class="artist-img"><img src="/images/artist/sally.png" alt=""></div>
			<div class="artist-img"><img src="/images/artist/scott.png" alt=""></div>
			<div class="artist-img"><img src="/images/artist/wirru.png" alt=""></div>
			<div class="artist-img"><img src="/images/artist/youru.png" alt=""></div>
		</div>
	</div>
</div>
<!-- end of artist -->
<!-- gallery -->
<div class="section bg-img" id="gallery">
	<div class="gallery">
		<h2 class="f-black">Asia Comic Con 2018</h2>
		<div class="divider red"></div>
		<div class="slider">
			<img src="images/gallery/1.jpg" alt="Asia Comic Con 2018">
			<img src="images/gallery/2.jpg" alt="Asia Comic Con 2018">
			<img src="images/gallery/3.jpg" alt="Asia Comic Con 2018">
			<img src="images/gallery/4.jpg" alt="Asia Comic Con 2018">
		</div>
	</div>
</div>
<!-- end of gallery -->
<!-- partner part-->
<div class="section bg-red" id="partners">
	<div class="partner-content">
		<h2 class="f-red">Official Partner</h2>
		<div class="divider blue"></div>
		<div class="partnerlogo"><img src="images/partner/hashbro.png" alt=""></div>
		<div class="partnerlogo"><img src="images/partner/maybank.png" alt=""></div>
		<div class="partnerlogo"><img src="images/partner/pearl.png" alt=""></div>
		<div class="partnerlogo"><img src="images/partner/bisclub.png" alt=""></div>
		<h2 class="f-blue">Official Media Partner</h2>
		<div class="divider red"></div>
		<div class="partnerlogo"><img src="images/partner/gs.png" alt=""></div>
		<div class="partnerlogo"><img src="images/partner/wanuxi.png" alt=""></div>
		<div class="partnerlogo"><img src="images/partner/gb.png" alt=""></div>
	</div>
</div>
<!-- end of partner part -->
<!-- pearl -->
<div class="section bg-img" id="pearl">
	<div class="gallery">
		<h2 class="f-black">Official Exclusive Hotel Partner</h2>
		<div class="divider red"></div>
		<div class="pearlimg">
			<img src="images/pearl.jpeg" alt="">
		</div>
		<a href="http://www.pearl.com.my/ " target="_blank">
			<div class="acc-button red">Book Now</div>
		</a>
	</div>
</div>
<!-- end of pearl -->
<!-- video -->
<div class="section bg-red" id="video">
	<h2 class="f-white">Video</h2>
	<div class="divider blue"></div>
	<div class="fb-video">
		<div class="videoWrapper">
			<iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fasiacomicconmalaysia%2Fvideos%2F1808828709199211%2F&show_text=0&width=100" width="100" height="auto" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allowFullScreen="true"></iframe>
		</div>
	</div>
</div>
<!-- end of video -->
<!-- booth -->
<div class="section bg-img" id="booth">
	<div class="booth">
		<h2 class="f-black">Booth Registration</h2>
		<div class="divider green"></div>
		<p class="f-black">Booth registration is officially OPEN now!
			<br>
		Book now!</p>
		<div class="booth-column">
			<div class="icon"><img src="images/booth/basicbooth.jpeg" alt="Booth Register"></div>
			<h4>Standard Booth Registration</h4>
			<a id="standard" href="https://forms.gle/hYvorC2kFhjsyiw78" target="_blank">
				<div class="acc-button red">Register Here</div>
			</a>
		</div>
		<div class="booth-column">
			<div class="icon"><img src="images/booth/minibooth.jpeg" alt="Booth Register"></div>
			<h4>Mini Booth<br>Registration</h4>
			<a id="mini" href="https://forms.gle/hYvorC2kFhjsyiw78" target="_blank">
				<div class="acc-button red">Register Here</div>
			</a>
		</div>
		<div class="booth-column">
			<div class="icon"><img src="images/booth/doujinbooth.jpeg" alt="Booth Register"></div>
			<h4>Doujin/Cosplay Booth Registration</h4>
			<a id="doujin-cosplay" href="https://forms.gle/LNg5Zjdqg2HdhFkw6" target="_blank">
				<div class="acc-button red">Register Here</div>
			</a>
		</div>
	</div>
</div>
<!-- end of booth -->
<!-- contact part-->
<div class="section bg-red" id="contact">
	<h2 class="f-white">Contact</h2>
	<div class="divider blue"></div>
	<div class="info f-white">
		<h3>I Hub Exhibitions Sdn Bhd (1304554-H)</h3><br><br>
		<p>Phone: +016-3977528</p>
		<p>Email: sales@asiacomiccon.com.my</p>
		<p>Address: L-07-01, Level 7, Block L, Solaris Mont Kiara, No.2, Jalan Solaris Mont Kiara, 50480 Kuala Lumpur.</p>
	</div>
</div>
<!-- end of contact part -->
<div class="section bg-img" id="contact">
	<div class="booth">
		<h2 class="f-black">Refund Policy</h2>
		<div class="divider blue"></div>
		<div class="info f-black">
			<p>All purchases are not returnable, refundable and exchangeable.</p>
		</div>
	</div>
</div>
@endsection