-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Aug 24, 2019 at 04:42 AM
-- Server version: 5.6.44-86.0
-- PHP Version: 7.2.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hrpoints`
--

-- --------------------------------------------------------

--
-- Table structure for table `tickets`
--

CREATE TABLE `tickets` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantity` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tickets`
--

INSERT INTO `tickets` (`id`, `name`, `description`, `price`, `quantity`, `code`) VALUES
(1, 'VIP ULTIMATE BUNDLE - LIMITED 1,000 pieces ONLY!', '1 x Early Bird Solo 2-day Ticket\n1 x ACC 2019 Pins\n1 x ACC Mug\n1 x Acc T-Shirt (Free Size)\nHasbro Exclusive Goodiebag\n+ Priority Queue for Guest Photo & Signature, VIP Concert Zone, Rest Area & Charging Zone', '239', '1000', 'EAP'),
(2, 'Solo 1-day Ticket (26 October 2019)', 'Solo 1-day entry ticket for 26 October 2019', '30', '10000000', 'STA'),
(3, 'Solo 1-day Ticket (27 October 2019)', 'Solo 1-day entry ticket for 27 October 2019', '30', '10000000', 'STB'),
(4, 'Solo 2-days Ticket', 'Solo 2-days ticket for 26 & 27 October 2019', '60', '10000000', 'STC'),
(5, 'Student 1-day Ticket (26 October 2019)', 'Student 1-day entry ticket for 26 October 2019', '20', '10000000', 'PTA'),
(6, 'Student 1-day Ticket (27 October 2019)', 'Student 1-day entry ticket for 26 October 2019', '20', '10000000', 'PTB'),
(7, 'Student 2-days Ticket', 'Student 2-days entry ticket for 26 & 27 October 2019', '40', '10000000', 'PTC'),
(8, 'Couple 1-day Ticket (26 October 2019)', 'Couple 1-day entry ticket for 26 October 2019', '55', '10000000', 'CTA'),
(9, 'Couple 1-day Ticket (27 October 2019)', 'Couple 1-day entry ticket for 27 October 2019', '55', '10000000', 'CTB'),
(10, 'Couple 2-days entry ticket for 26 & 27 October 2019', 'Couple 2-days entry ticket for 26 & 27 October 2019', '110', '10000000', 'CTC'),
(11, '5 Friends Group 1-day for 26 October 2019', '5 Friends 1-day entry ticket for 26 October 2019', '125', '10000000', 'GTA'),
(12, '5 Friends Group 1-day for 27 October 2019', '5 Friends 1-day entry ticket for 27 October 2019', '125', '10000000', 'GTA'),
(13, '5 Friends Group 2-days for 26 & 27 October 2019', '5 Friends 2-days entry ticket for 26 & 27 October 2019', '250', '10000000', 'GTC');
(14, 'iPay88 Testing Tickets', 'iPay88 Testing Tickets', '1', '10000000', 'IP88');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tickets`
--
ALTER TABLE `tickets`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tickets`
--
ALTER TABLE `tickets`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
