@extends('base')

@section('main')
<div class="acc-wrapper">
	<!-- start of nav -->
	<div class="nav">
		<div class="logo logotrigger"><img src="/images/logo/logo.png" alt="Asia Comic Con"></div>
		<div class="nav-toggle" id="menu-toggle"><img src="/images/icon/toggle.png" alt="Menu"></div>
		<div class="navigation-mobile">
			<div class="close-menu" id="menu-close"><img src="/images/icon/close.png" alt="Close"></div>
			<div class="mobile-nav-wrapper">
				<a href="/">
					<div class="nav-button about-trigger">Home</div>
				</a>
				@auth
				<a class="dropdown-item" href="{{ route('dashboard', auth()->user()->id) }}" >
					<!-- <div class="nav-button booth-trigger">{{ Auth::user()->name }}</div> -->
					<div class="nav-button booth-trigger">My Account</div>
				</a>
				<a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
					<div class="nav-button booth-trigger">{{ __('Logout') }}</div>
				</a>
				<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
					@csrf
				</form>
				@endauth
			</div>
		</div>
		<div class="navigation-desktop">
			<a href="/">
				<div class="nav-button about-trigger">Home</div>
			</a>
			@auth
			<a class="dropdown-item" href="{{ route('dashboard', auth()->user()->id) }}" >
				<!-- <div class="nav-button booth-trigger">{{ Auth::user()->name }}</div> -->
				<div class="nav-button booth-trigger">My Account</div>
			</a>
			<a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
				<div class="nav-button booth-trigger">{{ __('Logout') }}</div>
			</a>
			<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
				@csrf
			</form>
			@endauth
		</div>
	</div>
</div>

<div class="section bg-red full-height" style="margin-top:90px;">
	<div class="row">
		<div class="col-sm-12">
			<h1 class="display-3 f-white">Tickets</h1>
			<div class="divider blue"></div>
			@if (Session::get('vip') == 0)
			<div id="vip-limited" class="w3-panel w3-green w3-display-container">
				<p>{{ session()->get( 'message' ) }}</p>
			</div>
			@endif
			<form id="payment-form" method="POST">
				<div class='ticket-wrapper'>
					@foreach($tickets as $key => $ticket)
					<div class='ticket'>
						<div class="ticket-title"><p>{{$ticket->name}}</p><input id="ticket_{{$ticket->id}}_name" name="ticket_{{$ticket->id}}_name" type="hidden" value="{{$ticket->name}}"></div>
						<div class="ticket-desc"><p>{!! nl2br(e($ticket->description)) !!}</p></div>
						<div data-ticket-price="{{$ticket->price}}" class="price"><p>RM {{$ticket->price}}</p></div>

						<div class="ticket-quantity"><input id="ticket_{{$ticket->id}}_q" class="ticket_input" type="number" name="ticket_{{$ticket->id}}_q" min="0" max="30" value="0"></div>

						<div class="ticket_price"><p>RM <span>0</span></p></div>
						<input id="ticket_{{$ticket->id}}" class="ticket_price_input" name="ticket_{{$ticket->id}}_t" type="hidden" value="0">
					</div>
					@endforeach

				</div>

				<input id="ticket_total" type="hidden" name="ticket_t">
				@auth
				<input id="user_name" type="hidden" name="user_name" value="{{ Auth::user()->name }}">
				<input id="user_email" type="hidden" name="user_email" value="{{ Auth::user()->email }}">
				@endauth

				<div class="order_detail">
					<p>Total: RM <span id=grand_total>0</span></p>
				</div>
				<div class="end">
					@guest

					<!-- <a class="nav-link" href="{{ route('login') }}"><p id="logintoproceed" class="acc-button btn btn-primary">Login to Purchase</p></a> -->
					<div id="logintoproceed" class="acc-button btn btn-primary">Login to Purchase</div>
					@endguest
					@auth
					<div class="payvia"><h2 class="f-white">Pay via</h2></div>
					<!-- <input class="acc-button payment" type="submit" value="Pay via PayPal" formaction="{!! URL::to('paypal') !!}"> -->
					<input style="width: 140px; outline: none;"type="image" src="/images/payment/ipay88.png" formaction="{!! URL::to('ipay88') !!}">
					@endauth
				</div>
			</form>
		</div>
	</div>
</div>

<div id="login-SSO" class="card" style="display: none">
	<h2 class="display-3">{{ __('Login') }}</h2>

	<div class="card-body">
		<div class="sso facebook">
			<a href="{{ url('/auth/redirect/facebook') }}" class="btn btn-primary"><img src="/images/login/facebook.png"></a>
		</div>
		<div class="sso google">
			<a href="{{ url('/auth/redirect/google') }}" class="btn btn-primary"><img src="/images/login/google.png"></a>
		</div>
	</div>
</div>
@endsection