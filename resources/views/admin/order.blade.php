@extends('base')

@section('main')
<div class="acc-wrapper">
	<!-- start of nav -->
	<div class="nav">
		<div class="logo logotrigger"><img src="/images/logo/logo.png" alt="Asia Comic Con"></div>
		<div class="nav-toggle" id="menu-toggle"><img src="/images/icon/toggle.png" alt="Menu"></div>
		<div class="navigation-mobile">
			<div class="close-menu" id="menu-close"><img src="/images/icon/close.png" alt="Close"></div>
			<div class="mobile-nav-wrapper">
				<a href="/">
					<div class="nav-button about-trigger">Home</div>
				</a>
				<a href="/superadmin/order">
					<div class="nav-button booth-trigger">Order</div>
				</a>
				<a href="/superadmin/summary">
					<div class="nav-button booth-trigger">Summary</div>
				</a>
			</div>
		</div>
		<div class="navigation-desktop">
			<a href="/">
				<div class="nav-button about-trigger">Home</div>
			</a>
			<a href="/superadmin/order">
				<div class="nav-button booth-trigger">Order</div>
			</a>
			<a href="/superadmin/summary">
				<div class="nav-button booth-trigger">Summary</div>
			</a>
		</div>
	</div>
</div>
<div class="section bg-red full-height" style="margin-top:90px;">
	<div class="row">
		<div class="col-sm-12">
			<h1 class="display-5 f-white">Orders</h1>
			<div class="divider blue"></div>
			<table class="table">
				@foreach ($orders as $order_id => $tickets_list)
				<tr>
					<th colspan="2" style="background-color: #dbdbdb" align="left">
						ORDER ID: {{ $order_id }} <br>TOTAL: RM {{ $tickets_list[0]->amount}}
					</th>
					<th style="background-color: #dbdbdb; padding-top: 20px;" align="right">DATE: {{ date('d/m/y', strtotime($tickets_list[0]->created_at))}} @ {{date('H:i', strtotime($tickets_list[0]->created_at))}}</th>
				</tr>
				<tr>
					<th align="left">Ticket Name</th>
					<th align="center">Quantity</th>
					<th align="center">Amount</th>
				</tr>
				@foreach ($tickets_list as $ticket)
				<tr>
					<td align="left">{{ $ticket->name }}</td>
					<td align="center">{{ $ticket->quantity }}</td>
					<td align="center">RM {{ $ticket->total }}</td>
				</tr>
				@endforeach
				@endforeach
			</table>
		</div>
	</div>
</div>
@endsection