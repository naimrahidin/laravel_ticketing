@extends('base')

@section('main')
<div class="acc-wrapper">
	<!-- start of nav -->
	<div class="nav">
		<div class="logo logotrigger"><img src="/images/logo/logo.png" alt="Asia Comic Con"></div>
		<div class="nav-toggle" id="menu-toggle"><img src="/images/icon/toggle.png" alt="Menu"></div>
		<div class="navigation-mobile">
			<div class="close-menu" id="menu-close"><img src="/images/icon/close.png" alt="Close"></div>
			<div class="mobile-nav-wrapper">
				<a href="/">
					<div class="nav-button about-trigger">Home</div>
				</a>
				<a href="/superadmin/order">
					<div class="nav-button booth-trigger">Order</div>
				</a>
				<a href="/superadmin/summary">
					<div class="nav-button booth-trigger">Summary</div>
				</a>
			</div>
		</div>
		<div class="navigation-desktop">
			<a href="/">
				<div class="nav-button about-trigger">Home</div>
			</a>
			<a href="/superadmin/order">
				<div class="nav-button booth-trigger">Order</div>
			</a>
			<a href="/superadmin/summary">
				<div class="nav-button booth-trigger">Summary</div>
			</a>
		</div>
	</div>
</div>
<div class="section bg-red full-height" style="margin-top:90px;">
	<div class="row">
		<div class="col-sm-12">
			<h1 class="display-5 f-white">Summary</h1>
			<div class="divider blue"></div>
			<table class="table">
				<tr>
					<th colspan="3" style="background-color: #dbdbdb" align="left">
						Grand Summary
					</th>
				</tr>
				<tr>
					<th align="left">Item Name</th>
					<th align="center">Quantity</th>
					<th align="center">Amount</th>
				</tr>
				<tr>
					<td align="left">ACC HERO PACK</td>
					<td align="center">{{ $ticket1['quantity'] }}</td>
					<td align="center">RM {{ $ticket1['total'] }}</td>
				</tr>
				<tr>
					<td align="left">Solo 1-day Ticket (26 October 2019)</td>
					<td align="center">{{ $ticket2['quantity'] }}</td>
					<td align="center">RM {{ $ticket2['total'] }}</td>
				</tr>
				<tr>
					<td align="left">Solo 1-day Ticket (27 October 2019)</td>
					<td align="center">{{ $ticket3['quantity'] }}</td>
					<td align="center">RM {{ $ticket3['total'] }}</td>
				</tr>
				<tr>
					<td align="left">Solo 2-days Ticket</td>
					<td align="center">{{ $ticket4['quantity'] }}</td>
					<td align="center">RM {{ $ticket4['total'] }}</td>
				</tr>
				<tr>
					<td align="left">Pair 1-day Ticket (26 October 2019)</td>
					<td align="center">{{ $ticket7['quantity'] }}</td>
					<td align="center">RM {{ $ticket7['total'] }}</td>
				</tr>
				<tr>
					<td align="left">Pair 1-day Ticket (27 October 2019)</td>
					<td align="center">{{ $ticket8['quantity'] }}</td>
					<td align="center">RM {{ $ticket8['total'] }}</td>
				</tr>
				<tr>
					<td align="left">Pair 2-days Ticket</td>
					<td align="center">{{ $ticket5['quantity'] }}</td>
					<td align="center">RM {{ $ticket5['total'] }}</td>
				</tr>
				<tr>
					<td align="left">5 Friends Group 1-day Ticket (26 October 2019)</td>
					<td align="center">{{ $ticket9['quantity'] }}</td>
					<td align="center">RM {{ $ticket9['total'] }}</td>
				</tr>
				<tr>
					<td align="left">5 Friends Group 1-day Ticket (27 October 2019)</td>
					<td align="center">{{ $ticket10['quantity'] }}</td>
					<td align="center">RM {{ $ticket10['total'] }}</td>
				</tr>
				<tr>
					<td align="left">5 Friends Group 2-days Ticket</td>
					<td align="center">{{ $ticket8['quantity'] }}</td>
					<td align="center">RM {{ $ticket8['total'] }}</td>
				</tr>
				<tr>
					<td align="left">Total</td>
					<td align="center">{{ $ticket1['quantity'] + $ticket2['quantity'] + $ticket3['quantity'] + $ticket4['quantity'] + $ticket5['quantity'] + $ticket6['quantity'] + $ticket7['quantity'] + $ticket8['quantity'] + $ticket9['quantity'] + $ticket10['quantity']  }}</td>
					<td align="center">RM{{ $ticket1['total'] + $ticket2['total'] + $ticket3['total'] + $ticket4['total'] + $ticket5['total'] + $ticket6['total'] + $ticket7['total'] + $ticket8['total'] + $ticket9['total'] + $ticket10['total'] }}</td>
				</tr>
			</table>

			<table class="table">
				<tr>
					<th colspan="2" style="background-color: #dbdbdb" align="left">
						Purchase by Day
					</th>
				</tr>
				<tr>
					<th align="left">Item Name</th>
					<th align="center">Quantity</th>
				</tr>
				<tr>
					<td align="left">Day 1</td>
					<td align="center">{{ $D1 }}</td>
				</tr>
				<tr>
					<td align="left">Day 2</td>
					<td align="center">{{ $D2 }}</td>
				</tr>
			</table>
		</div>
	</div>
</div>
@endsection