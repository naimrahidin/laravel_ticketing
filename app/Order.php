<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
	public $timestamps = false;

	protected $fillable = [
		'ticket_id',
		'quantity',
		'total',
		'user_id'
	];
}
