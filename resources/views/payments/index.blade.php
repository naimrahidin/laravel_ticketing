@extends('base')

@section('main')
<div class="row">
	<div class="col-sm-12">
		<h1 class="display-3">Checkout</h1>
		<table class="table table-striped">
			<thead>
				<tr>
					<td>Ticket Type</td>
					<td>Quantity</td>
					<td>Total</td>
				</tr>
			</thead>
			<tbody>
				<?php $total = 0; ?>
				@foreach($orders as $order)
				<?php $total += $order->total ?>
				<tr>
					<td>{{$order->name}}</td>
					<td id="quantity_{{$order->ticket_id}}">{{$order->quantity}}</td>
					<td>{{$order->total}}</td>
				</tr>

				@endforeach
			</tbody>
		</table>

		@if($orders[0]->ticket_id == 1)
		<div>
			<p>Select your Free T-Shirt Here</p>
			<tbody>
				<tr>
					<td>M: <input id="shirt_M" class="shirts_input shirts_input_1" type="number" min="0" max="10" value=0></td>
					<td>L: <input id="shirt_L" class="shirts_input shirts_input_2" type="number" min="0" max="10" value=0></td>
					<td>XL: <input id="shirt_XL" class="shirts_input shirts_input_3" type="number" min="0" max="10" value=0></td>
					<td>XXL: <input id="shirt_XXL" class="shirts_input shirts_input_4" type="number" min="0" max="10" value=0></td>
				</tr>
			</tbody>
		</div>
		@endif

		<div class="order_detail">
			<p>Total: RM <span id=grand_total>{{$total}}</span></p>
			<p id="payment" class="btn btn-primary">Proceed to Payments</p>
		</div>
	</div>
</div>
@endsection