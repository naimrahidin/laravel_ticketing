<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;

/** All Paypal Details class **/
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Rest\ApiContext;
use Redirect;
use Session;
use URL;

/** All iPay88 Details class**/
use Ipay88\Ipay88\Payment as Payment88;

/** database **/
use App\Order;
use Auth;
use App\Ticket;
use App\ticketPayment;
use App\ticketOrder;


class TicketPaymentController extends Controller
{
    private $_api_context;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        /** PayPal api context **/
        $paypal_conf = \Config::get('paypal');
        $this->_api_context = new ApiContext(new OAuthTokenCredential($paypal_conf['client_id'], $paypal_conf['secret']));
        $this->_api_context->setConfig($paypal_conf['settings']);

        // $request = new \IPay88\Payment\Request('JjqeNo4clY');

        // $ipay88_conf = \Config::get('ipay88');
        // $this->_iPay88_merchant_code = $ipay88_conf['merchant_code'];
        // $this->_iPay88_merchant_key = $ipay88_conf['merchant_key'];
    }

    public function index(){
        $response = 1;
        return view('tickets.index', compact('response'));
    }

    public function createOrder($input, $merchant) {

        // $input = $request->all();
        $order_id = Order::all()->last()->id + 1;
        $order_time = Carbon::now();
        $user_id = Auth::id();
        $grand_total = 0;
        // $total = 0;

        for($i = 1; $i < 11; $i++){
            if($input['ticket_'.$i.'_q'] != 0) {

                $order = new Order();
                $order->id = $order_id;
                $order->ticket_id = $i;
                $order->quantity = (int)$input['ticket_'.$i.'_q'];
                $order->total = (int)$input['ticket_'.$i.'_t'];
                $order->user_id = $user_id;
                $order->order_time = $order_time;
                $order->save();

                $grand_total = $grand_total + $order->total;
            }

        }

        $this->createPayment($user_id, $order_id, $merchant, $grand_total);
    }

    public function createPayment($user_id, $order_id, $merchant, $total) {

        $payment = new ticketPayment();
        $payment->user_id = $user_id;
        $payment->order_id = $order_id;
        $payment->merchant = $merchant;
        $payment->amount = $total;
        $payment->save();

        Session::put('ticketPayment_id', $payment->id);
    }

    public function updatePayment($payment_id, $status) {

        $payment = ticketPayment::find($payment_id);
        $payment->payment_status = $status;
        $payment->save();

        if($status == 'Success') {
            $this->recordTicketOrder($payment_id);
        }
    }

    public function recordTicketOrder($payment_id) {

        $ticket_payment = ticketPayment::where('id',$payment_id)->first();
        $order_id = $ticket_payment->order_id;
        $user_id = $ticket_payment->user_id;

        $orders = Order::where('id', $order_id)->get();

        // $VIP_last_ticket = 'VIP-0000';
        // $D1_last_ticket = 'D1-SO-0';
        // $D2_last_ticket = 'D2-SO-0';
        // $VIP_just_the_number = substr(strrchr(rtrim($VIP_last_ticket, '-'), '-'), 1);
        // $D1_just_the_number = substr(strrchr(rtrim($D1_last_ticket, '-'), '-'), 1);
        // $D2_just_the_number = substr(strrchr(rtrim($D2_last_ticket, '-'), '-'), 1);

        $VIP_last_ticket = ticketOrder::where('ticket_number','LIKE', '%VIP%' )->latest('id')->first();
        $D1_last_ticket = ticketOrder::where('ticket_number','LIKE', '%D1%' )->latest('id')->first();
        $D2_last_ticket = ticketOrder::where('ticket_number','LIKE', '%D2%' )->latest('id')->first();
        $VIP_just_the_number = substr(strrchr(rtrim($VIP_last_ticket->ticket_number, '-'), '-'), 1);
        $D1_just_the_number = substr(strrchr(rtrim($D1_last_ticket->ticket_number, '-'), '-'), 1);
        $D2_just_the_number = substr(strrchr(rtrim($D2_last_ticket->ticket_number, '-'), '-'), 1);
        $VIP_running_number = 0;
        $D1_running_number = 0;
        $D2_running_number = 0;

        foreach($orders as $order){

            $ticket_id = $order->ticket_id;
            $ticket_quantity = $order->quantity;

            if($ticket_id == 1) {

                for($i = 1; $i <= $ticket_quantity; $i++){

                    $VIP_running_number = $VIP_just_the_number + 1;


                    $ticketOrders = new ticketOrder();
                    $ticketOrders->user_id = $user_id;
                    $ticketOrders->order_id = $order_id;
                    $ticketOrders->ticket_payment_id = $payment_id;
                    $ticketOrders->ticket_number = 'VIP-'.str_pad($VIP_running_number, 4, '0', STR_PAD_LEFT);
                    $ticketOrders->save();

                    $VIP_running_number = $VIP_running_number;
                }

            } else if($ticket_id == 2) {
                //solo ticket on the 26 October

                for($i = 1; $i <= $ticket_quantity; $i++){

                    $D1_running_number = $D1_just_the_number + 1;


                    $ticketOrders = new ticketOrder();
                    $ticketOrders->user_id = $user_id;
                    $ticketOrders->order_id = $order_id;
                    $ticketOrders->ticket_payment_id = $payment_id;
                    $ticketOrders->ticket_number = 'D1-SO-'.str_pad($D1_running_number, 4, '0', STR_PAD_LEFT);
                    $ticketOrders->save();

                    $D1_just_the_number = $D1_running_number;
                }

            } else if($ticket_id == 3) {
                //solo ticket on the 27 october

                for($i = 1; $i <= $ticket_quantity; $i++){

                    $D2_running_number = $D2_just_the_number + 1;

                    $ticketOrders = new ticketOrder();
                    $ticketOrders->user_id = $user_id;
                    $ticketOrders->order_id = $order_id;
                    $ticketOrders->ticket_payment_id = $payment_id;
                    $ticketOrders->ticket_number = 'D2-SO-'.str_pad($D2_running_number, 4, '0', STR_PAD_LEFT);
                    $ticketOrders->save();

                    $D2_just_the_number = $D2_running_number;
                }

            } else if($ticket_id == 4) {
                //solo ticket for both days

                for($i = 1; $i <= $ticket_quantity; $i++){

                    for($j = 1; $j <= 2; $j++){

                        ${"D".$j."_running_number" } = ${"D".$j."_just_the_number" } + 1;

                        $ticketOrders = new ticketOrder();
                        $ticketOrders->user_id = $user_id;
                        $ticketOrders->order_id = $order_id;
                        $ticketOrders->ticket_payment_id = $payment_id;
                        $ticketOrders->ticket_number = 'D'.$j.'-SO-'.str_pad(${"D".$j."_running_number" }, 4, '0', STR_PAD_LEFT);
                        $ticketOrders->save();

                        ${"D".$j."_just_the_number" } = ${"D".$j."_running_number" };
                    }
                }

            } else if($ticket_id == 5) {
                //ticket for couple for both days
                for($i = 1; $i <= $ticket_quantity; $i++){

                    for($j = 1; $j <= 2; $j++){ //days repeater

                        for($k = 1; $k <= 2; $k++){ //couple repeater ie 2 tickets for couple

                            ${"D".$j."_running_number" } = ${"D".$j."_just_the_number" } + 1;

                            $ticketOrders = new ticketOrder();
                            $ticketOrders->user_id = $user_id;
                            $ticketOrders->order_id = $order_id;
                            $ticketOrders->ticket_payment_id = $payment_id;
                            $ticketOrders->ticket_number = 'D'.$j.'-CP-'.str_pad(${"D".$j."_running_number" }, 4, '0', STR_PAD_LEFT);
                            $ticketOrders->save();

                            ${"D".$j."_just_the_number" } = ${"D".$j."_running_number" };

                        }
                    }
                }

            } else if($ticket_id == 6) {
                //ticket for group on both days
                for($i = 1; $i <= $ticket_quantity; $i++){

                    for($j = 1; $j <= 2; $j++){

                        for($k = 1; $k <= 5; $k++){

                            ${"D".$j."_running_number" } = ${"D".$j."_just_the_number" } + 1;

                            $ticketOrders = new ticketOrder();
                            $ticketOrders->user_id = $user_id;
                            $ticketOrders->order_id = $order_id;
                            $ticketOrders->ticket_payment_id = $payment_id;
                            $ticketOrders->ticket_number = 'D'.$j.'-FR-'.str_pad(${"D".$j."_running_number" }, 4, '0', STR_PAD_LEFT);
                            $ticketOrders->save();

                            ${"D".$j."_just_the_number" } = ${"D".$j."_running_number" };
                        }
                    }
                }

            }  else if($ticket_id == 7) {
                //ticket for pair 1st day
                for($i = 1; $i <= $ticket_quantity; $i++){

                    for($k = 1; $k <= 2; $k++){

                        $D1_running_number = $D1_just_the_number + 1;

                        $ticketOrders = new ticketOrder();
                        $ticketOrders->user_id = $user_id;
                        $ticketOrders->order_id = $order_id;
                        $ticketOrders->ticket_payment_id = $payment_id;
                        $ticketOrders->ticket_number = 'D1-CP-'.$D1_running_number;
                        $ticketOrders->save();

                        $D1_just_the_number = $D1_running_number;
                    }
                }

            }  else if($ticket_id == 8) {
                //ticket for pair 2nd day
                for($i = 1; $i <= $ticket_quantity; $i++){

                    for($k = 1; $k <= 2; $k++){

                        $D2_running_number = $D2_just_the_number + 1;

                        $ticketOrders = new ticketOrder();
                        $ticketOrders->user_id = $user_id;
                        $ticketOrders->order_id = $order_id;
                        $ticketOrders->ticket_payment_id = $payment_id;
                        $ticketOrders->ticket_number = 'D2-CP-'.$D2_running_number;
                        $ticketOrders->save();

                        $D2_just_the_number = $D2_running_number;
                    }
                }

            }  else if($ticket_id == 9) {
                //ticket for group on 1st day
                for($i = 1; $i <= $ticket_quantity; $i++){

                    for($k = 1; $k <= 5; $k++){

                        $D1_running_number = $D1_just_the_number + 1;

                        $ticketOrders = new ticketOrder();
                        $ticketOrders->user_id = $user_id;
                        $ticketOrders->order_id = $order_id;
                        $ticketOrders->ticket_payment_id = $payment_id;
                        $ticketOrders->ticket_number = 'D1-FR-'.$D1_running_number;
                        $ticketOrders->save();

                        $D1_just_the_number = $D1_running_number;
                    }
                }

            }  else if($ticket_id == 10) {
                //ticket for group on 2nd day
                for($i = 1; $i <= $ticket_quantity; $i++){

                    for($k = 1; $k <= 5; $k++){

                        $D2_running_number = $D2_just_the_number + 1;

                        $ticketOrders = new ticketOrder();
                        $ticketOrders->user_id = $user_id;
                        $ticketOrders->order_id = $order_id;
                        $ticketOrders->ticket_payment_id = $payment_id;
                        $ticketOrders->ticket_number = 'D2-FR-'.$D2_running_number;
                        $ticketOrders->save();

                        $D2_just_the_number = $D2_running_number;
                    }
                }
            }
        }
    }


    public function payWithpaypal(Request $request)
    {
        $input = $request->all();
        $VIP = $this->checkVIPavailability($input);

        if($VIP['status']) {
            $this->createOrder($input, 'Paypal');

            $payer = new Payer();
            $payer->setPaymentMethod('paypal');

            $item_list = new ItemList();
            $list = array();

            for($i = 1; $i < 11; $i++){
                if((int)$input['ticket_'.$i.'_q'] != 0) {

                    $unit_price = $request->get('ticket_'.$i.'_t')/$request->get('ticket_'.$i.'_q');
                    $item = new Item();
                    $item->setName($request->get('ticket_'.$i.'_name')) /** item name **/
                    ->setCurrency('MYR')
                    ->setQuantity($request->get('ticket_'.$i.'_q'))
                    ->setPrice($unit_price); /** unit price **/

                    array_push($list, $item);
                }
            }

            $total = $request->get('ticket_t');

            $item_list = new ItemList();
            $item_list->setItems($list);

            $amount = new Amount();
            $amount->setCurrency('MYR')
            ->setTotal($total);

            $transaction = new Transaction();
            $transaction->setAmount($amount)
            ->setItemList($item_list)
            ->setDescription('Your purchase description is here.');

            $redirect_urls = new RedirectUrls();
            $redirect_urls->setReturnUrl(URL::to('status')) /** Specify return URL **/
            ->setCancelUrl(URL::to('status'));

            $payment = new Payment();
            $payment->setIntent('Sale')
            ->setPayer($payer)
            ->setRedirectUrls($redirect_urls)
            ->setTransactions(array($transaction));

            /** dd($payment->create($this->_api_context));exit; **/
            try {
                $payment->create($this->_api_context);
            } catch (\PayPal\Exception\PPConnectionException $ex) {
                if (\Config::get('app.debug')) {
                    \Session::put('error', 'Connection timeout');
                    return Redirect::to('/tickets');
                } else {
                    \Session::put('error', 'Some error occur, sorry for inconvenient');
                    return Redirect::to('/tickets');
                }
            }

            foreach ($payment->getLinks() as $link) {
                if ($link->getRel() == 'approval_url') {
                    $redirect_url = $link->getHref();
                    break;
                }
            }
            /** add payment ID to session **/
            Session::put('paypal_payment_id', $payment->getId());
            if (isset($redirect_url)) {
                /** redirect to paypal **/
                return Redirect::away($redirect_url);
            }
            \Session::put('error', 'Unknown error occurred');
            return Redirect::to('/tickets');
        } else {
            $response = 0;

            if($VIP['left'] != 0){
                $message = 'There are only '.$VIP['left'].' ACC HERO PACK tickets available';
            } else {
                $message = 'There is no more ACC HERO PACK tickets available';
            }
            return redirect()->back()->with('message', $message)->with('vip', 0);

            // return view('tickets.index', compact('tickets', 'message', 'response'));
        }
    }

    public function getPaymentStatus()
    {
        /** Get the payment ID before session clear **/
        $payment_id = Session::get('paypal_payment_id');
        $ticketPaymentID = Session::get('ticketPayment_id');
        /** clear the session payment ID **/
        Session::forget('paypal_payment_id');
        if (empty(Input::get('PayerID')) || empty(Input::get('token'))) {
            \Session::put('error', 'Payment failed');
            return Redirect::to('/tickets');
        }
        $payment = Payment::get($payment_id, $this->_api_context);
        $execution = new PaymentExecution();
        $execution->setPayerId(Input::get('PayerID'));
        /**Execute the payment **/
        $result = $payment->execute($execution, $this->_api_context);
        if ($result->getState() == 'approved') {

            // \Session::put('success', 'Payment success');

            $this->updatePayment($ticketPaymentID, 'Success'); //update the payment with the payment status

            // return Redirect::to('/tickets');

            $response['status'] = 1;
            return view('tickets.response', compact('response'));

        } else {

            \Session::put('error', 'Payment failed');
             $this->updatePayment($ticketPaymentID, 'Failed'); //update the payment with the payment status
             // return Redirect::to('/tickets');

             $response['status'] = 0;
             return view('tickets.response', compact('response'));
         }
     }

     public function payWithipay88(Request $request){

        $input = $request->all();
        $VIP = $this->checkVIPavailability($input);

        if($VIP['status']) {
            $description = null;
            $loop = 1;

            $this->createOrder($input, 'iPay88');

            for($i = 1; $i < 11; $i++){
                if((int)$input['ticket_'.$i.'_q'] != 0) {

                    if($loop != 1) {
                        $description .= ', ';
                    }
                    $description .= $input['ticket_'.$i.'_name'].' x '.$input['ticket_'.$i.'_q'];
                    $loop++;
                }
            }

            $request = new \IPay88\Payment\Request(env('IPAY88_MERCHANT_KEY',''));

            $this->_data = array(
                'merchantCode' => $request->setMerchantCode(env('IPAY88_MERCHANT_CODE','')),
                'refNo' => $request->setRefNo(time().''.mt_rand()),
                'amount' => $request->setAmount($input['ticket_t']),
                'currency' => $request->setCurrency('MYR'),
                'prodDesc' => $request->setProdDesc($description),
                'userName' => $request->setUserName($input['user_name']),
                'userEmail' => $request->setUserEmail($input['user_email']),
                'userContact' => $request->setUserContact(''),
                'remark' => $request->setRemark(''),
                'lang' => $request->setLang('UTF-8'),
                'signature' => $request->getSignature(),
                'responseUrl' => $request->setResponseUrl('https://www.asiacomiccon.com.my/response'),
                'backendUrl' => $request->setBackendUrl('https://www.asiacomiccon.com.my/backend')
            );

            \IPay88\Payment\Request::make(env('IPAY88_MERCHANT_KEY',''), $this->_data);
        } else {
            $response = 0;

            if($VIP['left'] != 0){
                $message = 'There are only '.$VIP['left'].' ACC HERO PACK tickets available';
            } else {
                $message = 'There is no more ACC HERO PACK tickets available';
            }
            return redirect()->back()->with('message', $message)->with('vip', 0);

        }


    }

    public function response()
    {

        $response = (new \IPay88\Payment\Response)->init(env('IPAY88_MERCHANT_CODE',''));
        $ticketPaymentID = Session::get('ticketPayment_id');

        if($response['status'] == 1) {
            $this->updatePayment($ticketPaymentID, 'Success');
        } else {
            $this->updatePayment($ticketPaymentID, 'Failed');
        }
        return view('tickets.response', compact('response'));


    }

    public function ipay88_backend()
    {
        echo "RECEIVEOK";
    }

    public function checkVIPavailability($input)
    {
        if($input['ticket_1_q'] > 0){

            $VIP = ticketOrder::where('ticket_number','LIKE', '%VIP%' )->get();
            $available = 500 - count($VIP);

            if($available >= $input['ticket_1_q']) {
                $result['status'] = true;
                return $result;
            } else {
                $result['status'] = false;
                $result['left'] = $available;
                return $result;
            }

        } else {
            $result['status'] = true;
            return $result;
        }
    }

    public function verifyRedeem(Request $request) {

        $input = $request->all();
        $ticket_payments = DB::table('ticket_payments')
        ->where('id', $input['ticketID'])
        ->first();

        if($ticket_payments->verified == false){
            $verify = ticketPayment::find($input['ticketID']);
            $verify->verified = true;
            $verify->verified_time = Carbon::now();
            $verify->save();

            $result['status'] = true;
            $return['verified_time'] = $ticket_payments->verified_time;
            return $result;
        } else {

            $result['status'] = false;
            $return['verified_time'] = $ticket_payments->verified_time;
            return $result;
        }
    }
}