
function openLoginPopup(){
	$('#login-SSO').css('display', 'block');
	var newWindowWidth = $(window).width();
	var $loginPopup = $('#login-SSO');

	$.colorbox({
		inline: true,
		href: $loginPopup,
		width: 350,
		height: 400,

		onClosed: function() {
			$('#login-SSO').css('display', 'none');
		},
	});

	/*if (newWindowWidth < 641) {
		$.colorbox({
			inline: true,
			href: $promoMobile,
			width: 338,
			height: 338,
		});
	} else {
		$.colorbox({
			inline: true,
			href: $promoDesktop,
			width: 1000,
			height: 522,
		});
	}*/
}

$('#logintoproceed').click(function(){
	openLoginPopup();
});


$('#verifyRedeem').click(function(){

	var data = {
		ticketID: $('span#verifyID').text(),
	};

	$.ajax({

		type:'POST',
		url:'/redeem',
		data: data,
		success: function (data) {

			if(data['status'] == true){
				$('#verifyRedeem').css('display','none');
				$('#redeemed').css('display','block').text('Tickets Redeemed');
			}
		}
	});
});

