@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Login via Single Sign On') }}</div>

                <div class="card-body">
                    <div class="form-group row mb-0">
                        <div class="col-md-2">
                            <a href="{{ url('/auth/redirect/facebook') }}" class="btn btn-primary"><i class="fa fa-facebook"></i> Facebook</a>
                        </div>
                        <div class="col-md-2">
                            <a href="{{ url('/auth/redirect/google') }}" class="btn btn-primary"><i class="fa fa-facebook"></i> Google</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection
