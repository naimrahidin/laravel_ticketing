<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use Auth;
use App\Ticket;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function ajaxRequestPost(Request $request)
    {
        $input = $request->all();
        $order_id = Order::all()->last()->id + 1;

        for($i = 1; $i < 14; $i++){
            if($input['ticket_'.$i.'_q'] != 0) {

                $order = new Order();
                $order->id = $order_id;
                $order->ticket_id = $i;
                $order->quantity = (int)$input['ticket_'.$i.'_q'];
                $order->total = (int)$input['ticket_'.$i.'_t'];
                $order->user_id = Auth::id();
                $order->save();

            }
        }
        $url =  route('checkout', ['id' => $order_id]);
        $response = response()->json(['success' => true, 'url' =>  $url]);
        return $response;
    }
}
