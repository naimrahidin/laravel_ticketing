<!DOCTYPE html>
<html>
<head>
	<style type="text/css">
		html, body, div, span, applet, object, iframe, h1, h2, h3, h4, h5, h6, p, blockquote, pre, a, abbr, acronym, address, big, cite, code, del, dfn, em, img, ins, kbd, q, s, samp, small, strike, strong, sub, sup, tt, var, b, u, i, center, dl, dt, dd, ol, ul, li, fieldset, form, label, legend, table, caption, tbody, tfoot, thead, tr, th, td, article, aside, canvas, details, embed, figure, figcaption, footer, header, hgroup, menu, nav, output, ruby, section, summary, time, mark, audio, video {
			margin: 0;
			padding: 0;
			border: 0;
			font: normal;
			vertical-align: baseline;
			box-sizing: border-box;
			-webkit-font-smoothing: antialiased;
			/* This needs to be set or some font faced fonts look bold on Mac in Chrome/Webkit based browsers. */
			-moz-osx-font-smoothing: grayscale;
			/* Fixes font bold issue in Firefox version 25+ on Mac */
		}

		/* HTML5 display-role reset for older browsers */
		article, aside, details, figcaption, figure, footer, header, hgroup, menu, main, nav, section {
			display: block;
		}

		body, html {
			height: 100%;
			line-height: 1;
			letter-spacing: 1px;
		}

		body {
			background-color: #ffffff;
		}

		@font-face {
			font-family: "bbbi";
			src: url("https://asiacomiccon.com.my/fonts/bbbi.eot?") format("eot"), url("https://asiacomiccon.com.my/fonts/bbbi.woff") format("woff"), url("https://asiacomiccon.com.my/fonts/bbbi.ttf") format("truetype"), url("https://asiacomiccon.com.my/fonts/bbbi.svg#bbbi") format("svg");
		}

		h1,
		h2,
		h3,
		h4 {
			font-family: bbbi;
			line-height: 1.4em;
			font-size: 20px;
			font-weight: bold;
		}

		.acc-wrapper .acc-header {
			height: 150px;
			/*margin: 50px 70px;*/
			background-image: url('https://asiacomiccon.com.my/images/header.png');
			padding-left: 100px;
		}

		.acc-wrapper .acc-header .logo img {
			width: 160px;
		}

		.detail {
			margin: 50px 40px;
		}

		.detail .title{
			margin-bottom: 10px;
		}

		.detail .user, .detail .order {
			background-color: #F2F2F2;
			padding: 20px;
		}

		.detail .order {
			margin-top: 20px;
		}

		.detail p {
			margin: 0;
		}

		.detail .contact {
			height: 150px;
		}

		.detail .contact h2 {
			font-size: 15px;
			font-weight: bold;
		}

		.detail .contact p {
			font-size: 12px;
		}

		.contact .left {
			float: left;
			width: 550px;
		}

		.contact .right {
			float: right;
			text-align: right;
		}

		.contact .right img {
			width: 150px;
		}
	</style>
</head>
<body>
	<div class="acc-wrapper">
		<!-- end of nav -->
		<div class="acc-header">
			<div class="logo"><img src="https://asiacomiccon.com.my/images/logo/logo.png" alt="Asia Comic Con 2019"></div>
		</div>
	</div>
	<div class="detail">
		<div class="user">
			<h2 class="title">Customer Detail</h2>
			<p>
				Name: {{ $user->name }}
				<br>
				Email: {{ $user->email }}
				<br>
				Login Via: {{ ucfirst($user->provider) }}
			</p>
		</div>

		<div class="order">
			<h2 class="title">Order Detail</h2>
			<p>
				ID: {{ $ticket_payments->id }}
				<br>
				Total: RM {{ $ticket_payments->amount}}
				<br>
				Time: {{ date('d/m/y', strtotime($ticket_payments->updated_at))}} @ {{date('H:i', strtotime($ticket_payments->updated_at))}}
			</p>
		</div>

		<div class="order">
			<h2 class="title">Order Summary</h2>
			<table class="table" width="100%">
				<tr>
					<th align="left" width="60%">Ticket Name</th>
					<th align="center" width="20%">Quantity</th>
					<th align="center" width="20%">Amount</th>
				</tr>
				@foreach ($orders as $order)
				<tr>
					<td align="left"><p>{{ $order->name }}</p></td>
					<td align="center"><p>{{ $order->quantity }}</p></td>
					<td align="center"><p>RM{{ $order->total }}</p></td>
				</tr>
				@endforeach
			</table>

		</div>
	</div>
</body>
</html>