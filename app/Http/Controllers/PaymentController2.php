<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
/** All Paypal Details class **/
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Rest\ApiContext;
use Redirect;
use Session;
use URL;
/** All iPay88 Details class**/
require_once base_path('vendor/karyamedia/ipay88/src/IPay88.php');
require_once base_path('vendor/karyamedia/ipay88/src/IPay88/Payment/Request.php');

/** database **/
use App\Order;
use Auth;
use App\Ticket;
use App\Payment;

class PaymentController extends Controller
{
    private $_api_context;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        /** PayPal api context **/
        $paypal_conf = \Config::get('paypal');
        $this->_api_context = new ApiContext(new OAuthTokenCredential($paypal_conf['client_id'], $paypal_conf['secret']));
        $this->_api_context->setConfig($paypal_conf['settings']);

        // $ipay88_conf = \Config::get('ipay88');
        // $this->_merchantCode->setConfig($ipay88_conf['merchant_code']);
        // $this->_merchantKey->setConfig($ipay88_conf['merchant_key']);
    }

    public function index(){

    }

    public function createOrder($input) {

        // $input = $request->all();
        $order_id = Order::all()->last()->id + 1;
        $order_time = Carbon::now();
        $user_id = Auth::id();
        $merchant = 'paypal';

        for($i = 1; $i < 14; $i++){
            if($input['ticket_'.$i.'_q'] != 0) {

                $order = new Order();
                $order->id = $order_id;
                $order->ticket_id = $i;
                $order->quantity = (int)$input['ticket_'.$i.'_q'];
                $order->total = (int)$input['ticket_'.$i.'_t'];
                $order->user_id = $user_id;
                $order->order_time = $order_time;
                $order->save();

                $grand_total =+ $order->total;

            }
        }

        $this->createPayment($user_id, $order_id, $merchant, $grand_total);
    }

    public function runningNumber() {

    }

    public function createPayment($user_id, $order_id, $merchant, $total) {

        $payment = new Payments();
        $payment->user_id = $user_id;
        $payment->order_id = $order_id;
        $payment->merchant = $merchant;
        $payment->amount = $total;
        $payment->save();

    }


    public function payWithpaypal(Request $request)
    {
        $input = $request->all();
        $this->createOrder($input);

        $payer = new Payer();
        $payer->setPaymentMethod('paypal');

        $item_list = new ItemList();
        $list = array();

        for($i = 1; $i < 14; $i++){
            if($input['ticket_'.$i.'_q'] != 0) {

                $item = new Item();
                $item->setName($request->get('ticket_'.$i.'_name')) /** item name **/
                ->setCurrency('MYR')
                ->setQuantity($request->get('ticket_'.$i.'_q'))
                ->setPrice($request->get('ticket_'.$i.'_t')); /** unit price **/

                array_push($list, $item);
            }
        }

        $total = $request->get('ticket_t');

        $item_list = new ItemList();
        $item_list->setItems($list);

        $amount = new Amount();
        $amount->setCurrency('MYR')
        ->setTotal($total);

        $transaction = new Transaction();
        $transaction->setAmount($amount)
        ->setItemList($item_list)
        ->setDescription('Your purchase description is here.');

        $redirect_urls = new RedirectUrls();
        $redirect_urls->setReturnUrl(URL::to('status')) /** Specify return URL **/
        ->setCancelUrl(URL::to('status'));

        $payment = new Payment();
        $payment->setIntent('Sale')
        ->setPayer($payer)
        ->setRedirectUrls($redirect_urls)
        ->setTransactions(array($transaction));

        /** dd($payment->create($this->_api_context));exit; **/
        try {
            $payment->create($this->_api_context);
        } catch (\PayPal\Exception\PPConnectionException $ex) {
            if (\Config::get('app.debug')) {
                \Session::put('error', 'Connection timeout');
                return Redirect::to('/tickets');
            } else {
                \Session::put('error', 'Some error occur, sorry for inconvenient');
                return Redirect::to('/tickets');
            }
        }
        foreach ($payment->getLinks() as $link) {
            if ($link->getRel() == 'approval_url') {
                $redirect_url = $link->getHref();
                break;
            }
        }
        /** add payment ID to session **/
        Session::put('paypal_payment_id', $payment->getId());
        if (isset($redirect_url)) {
            /** redirect to paypal **/
            return Redirect::away($redirect_url);
        }
        \Session::put('error', 'Unknown error occurred');
        return Redirect::to('/tickets');
    }

    public function getPaymentStatus()
    {
        /** Get the payment ID before session clear **/
        $payment_id = Session::get('paypal_payment_id');
        /** clear the session payment ID **/
        Session::forget('paypal_payment_id');
        if (empty(Input::get('PayerID')) || empty(Input::get('token'))) {
            \Session::put('error', 'Payment failed');
            return Redirect::to('/tickets');
        }
        $payment = Payment::get($payment_id, $this->_api_context);
        $execution = new PaymentExecution();
        $execution->setPayerId(Input::get('PayerID'));
        /**Execute the payment **/
        $result = $payment->execute($execution, $this->_api_context);
        if ($result->getState() == 'approved') {
            \Session::put('success', 'Payment success');
            return Redirect::to('/tickets');
        }
        \Session::put('error', 'Payment failed');
        return Redirect::to('/tickets');
    }

    public function payWithipay88(Request $request){
        $request = new Request88(env('IPAY88_MERCHANT_KEY',''));
        $this->_data = array(
            'merchantCode' => $request->setMerchantCode(env('IPAY88_MERCHANT_CODE','')),
            'paymentId' =>  $request->setPaymentId(1),
            'refNo' => $request->setRefNo('EXAMPLE0001'),
            'amount' => $request->setAmount('1.00'),
            'currency' => $request->setCurrency('MYR'),
            'prodDesc' => $request->setProdDesc('Testing'),
            'userName' => $request->setUserName('Your name'),
            // 'userEmail' => $request->setUserEmail('naimrahidin@gmail.com'),
            'userContact' => $request->setUserContact('0123456789'),
            'remark' => $request->setRemark('Some remarks here..'),
            'lang' => $request->setLang('UTF-8'),
            'signature' => $request->getSignature(),
            'responseUrl' => $request->setResponseUrl('http://example.com/response'),
            'backendUrl' => $request->setBackendUrl('http://example.com/backend')
        );

        IPay88\Payment\Request::make(env('IPAY88_MERCHANT_KEY',''), $this->_data);
    }

    public function response()
    {
        $response = (new IPay88\Payment\Response)->init($this->_merchantCode);
        echo "<pre>";
        print_r($response);
    }
}