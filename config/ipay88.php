<?php
return [
    'merchant_code' => env('IPAY88_MERCHANT_CODE',''),
    'merchant_key' => env('IPAY88_MERCHANT_KEY',''),
];