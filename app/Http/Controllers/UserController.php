<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\DB;
use Carbon;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
    	$user = User::find($id);

    	$orders = DB::table('ticket_payments')
        ->select('ticket_payments.*', 'tickets.name', 'orders.*')
        ->where('ticket_payments.user_id', $id)
        ->where('ticket_payments.payment_status', 'Success')
        ->join('orders', function ($join) {
            $join->on('ticket_payments.order_id', '=', 'orders.id');
        })
        ->join('tickets', 'orders.ticket_id', '=', 'tickets.id')
        ->orderBy('ticket_payments.id', 'desc')
        ->get()
        ->groupBy(function($item) {
            return $item->id;
        });


        /*
        ->where('user_id', $id)
        ->orderBy('id', 'asc')
        ->get()
        ->groupBy('id');*/
        // ->groupBy(function ($val) {
        //     return $val->id;
        // });

        /*foreach ($order_ids as $order_id => $orders) {
            echo '<h2>'.$order_id.'</h2><ul>';
            foreach ($orders as $order) {
                echo '<li>'.$order->ticket_id.'</li>';
            }
            echo '</ul>';
        }*/

        return view('users.index', compact('user', 'orders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function reportOrders()
    {
        $orders = DB::table('ticket_payments')
        ->select('ticket_payments.*', 'tickets.name', 'orders.*')
        ->join('orders', function ($join) {
            $join->on('ticket_payments.order_id', '=', 'orders.id')->where('ticket_payments.payment_status', '=', 'Success');
        })
        ->join('tickets', 'orders.ticket_id', '=', 'tickets.id')
        ->orderBy('ticket_payments.id', 'desc')
        ->get()
        ->groupBy(function($item) {
            return $item->id;
        });

        return view('admin.order', compact('orders'));
    }

    public function reportTickets()
    {
        $tickets = DB::table('ticket_orders')
        ->get();

        return view('admin.ticket', compact('tickets'));
    }

    public function reportSummary()
    {
        $D1 = 0;
        $D2 = 0;
        $VIP = 0;
        $ticket1 = [];
        $ticket2 = [];
        $ticket3 = [];
        $ticket4 = [];
        $ticket5 = [];
        $ticket6 = [];
        $ticket7 = [];
        $ticket8 = [];
        $ticket9 = [];
        $ticket10 = [];
        $ticket1_t = 0;
        $ticket1_q = 0;
        $ticket1_n = '';
        $ticket2_t = 0;
        $ticket2_q = 0;
        $ticket2_n = '';
        $ticket3_t = 0;
        $ticket3_q = 0;
        $ticket3_n = '';
        $ticket4_t = 0;
        $ticket4_q = 0;
        $ticket4_n = '';
        $ticket5_t = 0;
        $ticket5_q = 0;
        $ticket5_n = '';
        $ticket6_t = 0;
        $ticket6_q = 0;
        $ticket6_n = '';
        $ticket7_t = 0;
        $ticket7_q = 0;
        $ticket7_n = '';
        $ticket8_t = 0;
        $ticket8_q = 0;
        $ticket8_n = '';
        $ticket9_t = 0;
        $ticket9_q = 0;
        $ticket9_n = '';
        $ticket10_t = 0;
        $ticket10_q = 0;
        $ticket10_n = '';

        $ticket_orders = DB::table('ticket_orders')
        ->get();

        foreach ($ticket_orders as $ticket_number) {
            if(strpos($ticket_number->ticket_number, 'D1') !== false) {
                $D1++;

            } else if(strpos($ticket_number->ticket_number, 'D2') !== false) {
                $D2++;

            } else if(strpos($ticket_number->ticket_number, 'VIP') !== false) {
                $VIP++;
            }
        }

        $orders = DB::table('ticket_payments')
        ->select('ticket_payments.*', 'tickets.name', 'orders.*')
        ->join('orders', function ($join) {
            $join->on('ticket_payments.order_id', '=', 'orders.id')->where('ticket_payments.payment_status', '=', 'Success');
        })
        ->join('tickets', 'orders.ticket_id', '=', 'tickets.id')
        ->orderBy('ticket_payments.id', 'desc')
        ->get();

        foreach ($orders as $order) {

            if($order->ticket_id == 1) {

                $ticket1_n = $order->name;
                $ticket1_t += $order->total;
                $ticket1_q += $order->quantity;

            } elseif($order->ticket_id == 2) {

                $ticket2_n = $order->name;
                $ticket2_t += $order->total;
                $ticket2_q += $order->quantity;

            } elseif($order->ticket_id == 3) {
                $ticket3_n = $order->name;
                $ticket3_t += $order->total;
                $ticket3_q += $order->quantity;

            } elseif($order->ticket_id == 4) {
                $ticket4_n = $order->name;
                $ticket4_t += $order->total;
                $ticket4_q += $order->quantity;

            } elseif($order->ticket_id == 5) {
                $ticket5_n = $order->name;
                $ticket5_t += $order->total;
                $ticket5_q += $order->quantity;

            } elseif($order->ticket_id == 6) {
                $ticket6_n = $order->name;
                $ticket6_t += $order->total;
                $ticket6_q += $order->quantity;

            } elseif($order->ticket_id == 7) {
                $ticket7_n = $order->name;
                $ticket7_t += $order->total;
                $ticket7_q += $order->quantity;

            } elseif($order->ticket_id == 8) {
                $ticket8_n = $order->name;
                $ticket8_t += $order->total;
                $ticket8_q += $order->quantity;

            } elseif($order->ticket_id == 9) {
                $ticket9_n = $order->name;
                $ticket9_t += $order->total;
                $ticket9_q += $order->quantity;

            } elseif($order->ticket_id == 10) {
                $ticket10_n = $order->name;
                $ticket10_t += $order->total;
                $ticket10_q += $order->quantity;

            }

            $ticket1['total'] = $ticket1_t;
            $ticket1['quantity'] = $ticket1_q;
            $ticket1['name'] = $ticket1_n;
            $ticket2['total'] = $ticket2_t;
            $ticket2['quantity'] = $ticket2_q;
            $ticket2['name'] = $ticket2_n;
            $ticket3['total'] = $ticket3_t;
            $ticket3['quantity'] = $ticket3_q;
            $ticket3['name'] = $ticket3_n;
            $ticket4['total'] = $ticket4_t;
            $ticket4['quantity'] = $ticket4_q;
            $ticket4['name'] = $ticket4_n;
            $ticket5['total'] = $ticket5_t;
            $ticket5['quantity'] = $ticket5_q;
            $ticket5['name'] = $ticket5_n;
            $ticket6['total'] = $ticket6_t;
            $ticket6['quantity'] = $ticket6_q;
            $ticket6['name'] = $ticket6_n;
            $ticket7['total'] = $ticket7_t;
            $ticket7['quantity'] = $ticket7_q;
            $ticket7['name'] = $ticket7_n;
            $ticket8['total'] = $ticket8_t;
            $ticket8['quantity'] = $ticket8_q;
            $ticket8['name'] = $ticket8_n;
            $ticket9['total'] = $ticket9_t;
            $ticket9['quantity'] = $ticket9_q;
            $ticket9['name'] = $ticket9_n;
            $ticket10['total'] = $ticket10_t;
            $ticket10['quantity'] = $ticket10_q;
            $ticket10['name'] = $ticket10_n;
        }



        $grand_total = DB::table('ticket_payments')
        ->where('payment_status', '=', 'Success')
        ->sum('amount');

        return view('admin.summary', compact('D1', 'D2', 'VIP', 'grand_total', 'orders', 'ticket1', 'ticket2', 'ticket3', 'ticket4', 'ticket5', 'ticket6', 'ticket7', 'ticket8', 'ticket9', 'ticket10'));
    }
}
