@extends('base')

@section('main')
<div class="acc-wrapper">
	<!-- start of nav -->
	<div class="nav">
		<div class="logo logotrigger"><img src="/images/logo/logo.png" alt="Asia Comic Con"></div>
		<div class="nav-toggle" id="menu-toggle"><img src="/images/icon/toggle.png" alt="Menu"></div>
		<div class="navigation-mobile">
			<div class="close-menu" id="menu-close"><img src="/images/icon/close.png" alt="Close"></div>
			<div class="mobile-nav-wrapper">
				<a href="/">
					<div class="nav-button about-trigger">Home</div>
				</a>
				<a href="/admin/order">
					<div class="nav-button booth-trigger">Order</div>
				</a>
				<a href="/admin/ticket">
					<div class="nav-button booth-trigger">Ticket</div>
				</a>
			</div>
		</div>
		<div class="navigation-desktop">
			<a href="/">
				<div class="nav-button about-trigger">Home</div>
			</a>
			<a href="/admin/order">
				<div class="nav-button booth-trigger">Order</div>
			</a>
			<a href="/admin/ticket">
				<div class="nav-button booth-trigger">Ticket</div>
			</a>
		</div>
	</div>
</div>
<div class="section bg-red full-height" style="margin-top:90px;">
	<div class="row">
		<div class="col-sm-12">
			<h1 class="display-5 f-white">Reports</h1>
			<div class="divider blue"></div>
			<table class="table">
				<tr>
					<th align="center">Ticket ID</th>
					<th align="center">Created At</th>

				</tr>
				@foreach ($tickets as $ticket)
				<tr>
					<td align="left">{{ $ticket->ticket_number }}</td>
					<td align="center">{{date('d/m H:i', strtotime($ticket->created_at))}}</td>
				</tr>
				@endforeach
			</table>
		</div>
	</div>
</div>
@endsection