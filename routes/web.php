<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

Route::get('/auth/redirect/{provider}', 'SocialController@redirect');
Route::get('/callback/{provider}', 'SocialController@callback');

Route::get('/user/{id}', 'UserController@index')->name('dashboard');
Route::get('/superadmin/order', 'UserController@reportOrders');
Route::get('/superadmin/summary', 'UserController@reportSummary');



Route::resource('/tickets', 'TicketController');

// Route::post('createOrder', 'OrderController@ajaxRequestPost');
// Route::post('createOrder', 'TicketPaymentController@payWithpaypal');

// Route::get('/checkout/{id}', array('as' => 'checkout', 'uses' => 'TicketPaymentController@show'));
Auth::routes();

// Route::get('/logout', 'TicketController@index');

// Route::get('/home', 'HomeController@index')->name('home');

// route for processing payment
Route::post('paypal', 'TicketPaymentController@payWithpaypal');
Route::post('ipay88', 'TicketPaymentController@payWithipay88');
// route for check status of the payment
Route::get('status', 'TicketPaymentController@getPaymentStatus');

Route::post('response', 'TicketPaymentController@response');

Route::post('backend', 'TicketPaymentController@ipay88_backend');

Route::get('generate-pdf/{user}/{order}','HomeController@generatePDF');

Route::get('verify/{user}/{order}','HomeController@verify');
Route::post('redeem','TicketPaymentController@verifyRedeem');
