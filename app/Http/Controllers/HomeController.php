<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use PDF;
use QrCode;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function verify($userId, $orderId)
    {

        $ticket_payments = DB::table('ticket_payments')
        ->where('order_id', $orderId)
        ->first();

        $user = DB::table('users')
        ->where('id', $userId)
        ->first();

        $orders = DB::table('orders')
        ->select('orders.*', 'tickets.name')
        ->where('orders.id', $orderId)
        ->join('tickets', 'orders.ticket_id', '=', 'tickets.id')
        ->get();

        $ticket_orders = DB::table('ticket_orders')
        ->where('order_id', $orderId)
        ->orderBy('ticket_number', 'asc')
        ->get();

        return view('verify.index', compact('orders', 'ticket_payments', 'user', 'ticket_orders'));
    }

    public function generatePDF($userId, $orderId)
    {

        $ticket_payments = DB::table('ticket_payments')
        ->where('order_id', $orderId)
        ->first();

        $user = DB::table('users')
        ->where('id', $userId)
        ->first();

        $orders = DB::table('orders')
        ->select('orders.*', 'tickets.name')
        ->where('orders.id', $orderId)
        ->join('tickets', 'orders.ticket_id', '=', 'tickets.id')
        ->get();

        $ticket_orders = DB::table('ticket_orders')
        ->where('order_id', $orderId)
        ->orderBy('ticket_number', 'asc')
        ->get();

        $png = QrCode::format('png')->size(100)->generate('https://www.asiacomiccon.com.my/verify/'.$user->id.'/'.$orderId);
        // $png = QrCode::format('png')->size(100)->generate('http://b55bb932.ngrok.io/verify/'.$user->id.'/'.$orderId);

        $png = base64_encode($png);

        $pdf = PDF::loadView('myPDF', compact('orders', 'ticket_payments', 'user', 'ticket_orders', 'png'));
        return $pdf->download('Asia Comic Con Malaysia - Order '.$orderId.'.pdf');

        // return view('myPDF', compact('orders', 'ticket_payments', 'user', 'ticket_orders', 'png'));
    }
}