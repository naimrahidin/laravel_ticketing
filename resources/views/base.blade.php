<!doctype html>
<html class="no-js" lang="">

<head>
	<meta charset="utf-8">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="title" content="Asia Comic Con Malaysia">
	<meta name="description" content="The Unforgetable Pop Culture Experience is here!">
	<meta name="keywords" content="Asia Comic Con, ACC, Comic, Asia, Gaming, Cosplay, Card Games, Superstar, Toy & Collectible">
	<!-- fb metatag -->
	<meta property="og:url" content="https://www.asiacomiccon.com.my" />
	<meta property="og:type" content="website" />
	<meta property="og:title" content="Asia Comic Con Malaysia" />
	<meta property="og:description" content="The Unforgetable Pop Culture Experience is here!" />
	<meta property="og:image" content="https://www.asiacomiccon.com.my/images/fb-share.png" />
	<title>Asia Comic Con Malaysia</title>
	<link rel="apple-touch-icon" href="apple-touch-icon.png">
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
	<link rel="icon" href="/favicon.ico" type="image/x-icon">
	<!-- Place favicon.ico in the root directory -->
	<link rel="stylesheet" href="{{ asset('css/vendor.css') }}">
	<!-- <link rel="stylesheet" href="{{ asset('css/app.css') }}"> -->
	<link rel="stylesheet" href="{{ asset('css/main.css') }}">
	<link rel="stylesheet" href="{{ asset('css/custom.css') }}">
	<script src="{{ URL::asset('js/vendor/modernizr.js') }}"></script>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-148340037-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'UA-148340037-1');
	</script>
</head>

<body>
	@yield('main')
	<div class="footer">
		<p class="f-black disclaimer">Â© Asia Comic Con Malaysia 2019</p>
	</div>
	<script src="{{ URL::asset('js/vendor.js') }}"></script>
	<script src="{{ URL::asset('js/main.js') }}"></script>
	<script src="{{ URL::asset('js/app.js') }}"></script>
	<script src="{{ URL::asset('js/calculate.js') }}"></script>
	<script src="{{ URL::asset('js/payment.js') }}"></script>
	<script src="{{ URL::asset('js/custom.js') }}"></script>
	<script src="{{ URL::asset('js/vendor/colorbox.js') }}"></script>
</body>

</html>
