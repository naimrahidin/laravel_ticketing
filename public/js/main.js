'use strict';

$('#menu-toggle').click(function () {
	$('.navigation-mobile').addClass('active');
});

$('#menu-close').click(function () {
	$('.navigation-mobile').removeClass('active');
});

$('.mobile-nav-wrapper .nav-button').click(function () {
	$('.navigation-mobile').removeClass('active');
});

$(window).scroll(function () {
	if ($(this).scrollTop() > $(document).height() - $(window).height() - 2500) {
		$('.scrolltop').fadeIn();
	} else {
		$('.scrolltop').fadeOut();
	}
});

/*$('.scrolltop').click(function() {
  $('html, body').animate({
    scrollTop: $('body').offset().top - 70
  }, 1500);
});*/

$('.logotrigger').click(function () {
	$('html, body').animate({
		scrollTop: $('body').offset().top - 70
	}, 1500);
});

$('.activities-trigger').click(function () {
	$('html, body').animate({
		scrollTop: $('#activities').offset().top - 70
	}, 1500);
});

$('.artist-trigger').click(function () {
	$('html, body').animate({
		scrollTop: $('#artist').offset().top - 70
	}, 1500);
});

$('.ticket-trigger').click(function () {
	$('html, body').animate({
		scrollTop: $('#ticket').offset().top - 70
	}, 1500);
});

$('.about-trigger').click(function () {
	$('html, body').animate({
		scrollTop: $('#about').offset().top - 70
	}, 1500);
});

$('.video-trigger').click(function () {
	$('html, body').animate({
		scrollTop: $('#video').offset().top - 70
	}, 1500);
});

$('.gallery-trigger').click(function () {
	$('html, body').animate({
		scrollTop: $('#gallery').offset().top - 70
	}, 1500);
});

$('.booth-trigger').click(function () {
	$('html, body').animate({
		scrollTop: $('#booth').offset().top - 70
	}, 1500);
});

$('.slider').slick({
	dots: true,
	arrows: true,
	infinite: true,
	speed: 1000,
	fade: true,
	cssEase: 'linear',
	autoplay: true,
	autoplaySpeed: 3000
});